from tkinter import messagebox
from tkinter import *
import requests
import ast
import time


class Clock(Label):
    def __init__(self, parent=None, seconds=True, colon=False):
        Label.__init__(self, parent)
        self.display_seconds = seconds
        if self.display_seconds:
            self.time = time.strftime('%H:%M:%S')
        else:
            self.time = time.strftime('%I:%M %p').lstrip('0')
        self.display_time = self.time
        self.configure(text=self.display_time)
        if colon:
            self.blink_colon()
        self.after(200, self.tick)

    def tick(self):
        """ Updates the display clock every 200 milliseconds """
        if self.display_seconds:
            new_time = time.strftime('%H:%M:%S')
        else:
            new_time = time.strftime('%I:%M %p').lstrip('0')
        if new_time != self.time:
            self.time = new_time
            self.display_time = self.time
            self.config(text=self.display_time)
        self.after(200, self.tick)

    def blink_colon(self):
        """ Blink the colon every second """
        if ':' in self.display_time:
            self.display_time = self.display_time.replace(':', ' ')
        else:
            self.display_time = self.display_time.replace(' ', ':', 1)
        self.config(text=self.display_time)
        self.after(1000, self.blink_colon)


def button_log_in(*args):
    """"AFTER PRESS LOG IN open new window take your name and password  check if the pass contain just numbers and
    send it to anther function that check with the db"""
    frame_log_in = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_log_in.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)

    frame_x = Frame(welcome_window, bg='lightblue', highlightthickness=2, highlightbackground="#111")
    frame_x.place(relx=0.1, rely=0.25, relwidth=0.8, relheight=0.3)

    l = Label(frame_log_in, text='enter user name and password', bg='#80c1ff', font=("impact", 40))
    l.pack(side='top')

    l_name = Label(frame_x, text='user name:', font=("impact", 20), bg='lightblue', highlightthickness=5,
                   highlightbackground="#111")
    l_name.place(relx=0.34, rely=0.02, relwidth=0.3, relheight=0.2)

    global user_var
    user_var = StringVar()
    user_name = Entry(frame_x, textvariable=user_var, bg='white')
    user_name.place(relx=0.4, rely=0.2)

    l_password = Label(frame_x, text='password:', bd=3, font=("impact", 20), bg='lightblue', highlightthickness=5,
                       highlightbackground="#b3c6ff")
    l_password.place(relx=0.34, rely=0.35, relwidth=0.3, relheight=0.2)

    global pass_var
    pass_var = StringVar()
    password = Entry(frame_x, textvariable=pass_var, bg='white', show="*")
    password.place(relx=0.4, rely=0.55)

    b10 = Button(frame_x, text="continue", font=('impact', 20), command=check_if_in_db, bg='lightblue',
                 highlightthickness=2, highlightbackground="lightblue")
    b10.place(relx=0.43, rely=0.7, relwidth=0.1, relheight=0.1)

    b11 = Button(frame_log_in, text='Back', font=('impact', 20), bg='#80c1ff', command=main)
    b11.place(relx=0, rely=0, relwidth=0.05, relheight=0.05)


def check_if_in_db(*args):
    global color
    """the continue that of the button_log_in. this function send to the server the user name and the password
    and present the appropriate message"""
    u_name = user_var.get()
    password = pass_var.get()
    path = "https://nati-server.herokuapp.com/log_in_route"
    info = {'user-name': u_name, 'password': password}
    ans = requests.get(url=path, params=info)
    ans = ans.content
    dict_str = ans.decode("UTF-8")
    if dict_str != 'no user':
        mydata = ast.literal_eval(dict_str)
        color = mydata['color']
        open_user_page(mydata)
    else:
        messagebox.showinfo(welcome_window, 'you dont have user, please register or try again to log in',
                            command=button_log_in())


def open_user_page(user_info):
    """this is the frame that open after the check if the user name and the password are
     in the db and present the info about them"""

    first_name = user_info['first-name']
    last_name = user_info['last-name']
    user_name = user_info['user-name']
    job = user_info['job']
    clearance = user_info['clearance']

    frame_user_page = Frame(welcome_window, bg=color, highlightthickness=2, highlightbackground="#111")
    frame_user_page.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)

    frame_y = Frame(welcome_window, bg='#80ffff', highlightthickness=2, highlightbackground="#111")
    frame_y.place(relx=0.1, rely=0.2, relwidth=0.5, relheight=0.2)

    l_title = Label(frame_user_page, text='welcome Back %s' % user_name, bg=color, font=("impact", 44))
    l_title.pack(side='top')

    ly_title = Label(frame_y, text='your details', bg='#80ffff', font=("impact", 20))
    ly_title.pack(side='top')

    l_fname = Label(frame_y, text="first name: %s " % first_name, bg='#80ffff', font=("impact", 20))
    l_fname.place(relx=0.04, rely=0.15)

    l_lname = Label(frame_y, text="last name: %s " % last_name, bg='#80ffff', font=("impact", 20))
    l_lname.place(relx=0.04, rely=0.6)

    l_fname = Label(frame_y, text="job: %s " % job, bg='#80ffff', font=("impact", 20))
    l_fname.place(relx=0.6, rely=0.15)

    l_clearace = Label(frame_y, text="level of clearance is: %s " % clearance, bg='#80ffff',
                       font=("impact", 20))
    l_clearace.place(relx=0.6, rely=0.6)

    mb = Menubutton(frame_user_page, text="OPTIONS", relief=RAISED, bg=color, font=('impact', 20))
    mb.place(relx=0.05, rely=0.4, relheight=0.1, relwidth=0.2)
    mb.menu = Menu(mb, tearoff=0)
    mb["menu"] = mb.menu

    changevar = IntVar()
    msgvar = IntVar()

    mb.menu.add_checkbutton(label="Chats", variable=changevar, font=('impact', 20), command=msg_method)
    mb.menu.add_checkbutton(label="Update your details", variable=msgvar, font=('impact', 20), command=update)
    mb.menu.add_checkbutton(label="change background", variable=msgvar, font=('impact', 20), command=background)
    mb.menu.add_checkbutton(label="LOG OUT", variable=msgvar, font=('impact', 20), command=main)

    clock1 = Clock(frame_user_page)
    clock1.place(relx=0, rely=0)
    clock1.configure(bg=color, fg='black', font=("helvetica", 35))


def background(*args):
    """here the user choose the color background"""
    global var, choose_var
    var = IntVar()
    frame_background = Frame(welcome_window, bg=color, highlightthickness=2, highlightbackground="#111")
    frame_background.place(relx=0.6, rely=0.2, relwidth=0.3, relheight=0.7)
    l_fname = Label(frame_background, text="choose the background", bg=color, font=("impact", 30))
    l_fname.pack(side='top')

    r1 = Radiobutton(frame_background, text="light blue", variable=var, value=1, font=('impact', 15), bg='#80c1ff')
    r1.place(relx=0.2, rely=0.3, relwidth=0.28, relheight=0.1)

    r2 = Radiobutton(frame_background, text="light red", variable=var, value=2, font=('impact', 15), bg='#ff6666')
    r2.place(relx=0.2, rely=0.4, relwidth=0.28, relheight=0.1)

    r3 = Radiobutton(frame_background, text="light green", variable=var, value=3, font=('impact', 15), bg='#8cff66')
    r3.place(relx=0.2, rely=0.5, relwidth=0.28, relheight=0.1)

    r4 = Radiobutton(frame_background, text="light yellow", variable=var, value=4, font=('impact', 15), bg='#ffff4d')
    r4.place(relx=0.2, rely=0.6, relwidth=0.28, relheight=0.1)

    r5 = Radiobutton(frame_background, text="light pink", variable=var, value=5, font=('impact', 15), bg='#ff80d5')
    r5.place(relx=0.2, rely=0.7, relwidth=0.28, relheight=0.1)

    b11 = Button(frame_background, text='update', font=('impact', 15), bg=color, command=send_to_db_color)
    b11.place(relx=0.55, rely=0.5, relwidth=0.14, relheight=0.07)

    b12 = Button(frame_background, text='X', font=('impact', 15), bg=color, command=check_if_in_db)
    b12.place(relx=0, rely=0, relwidth=0.06, relheight=0.04)


def send_to_db_color(*args):
    """send to the db the color the user chooose"""
    color = var.get()
    user_name = user_var.get()
    path = "https://nati-server.herokuapp.com/background"
    info = {'color': color, 'user-name': user_name}
    answer = requests.get(url=path, params=info)
    ans1 = answer.content
    ans1 = ans1.decode("utf-8")
    if '#' in ans1:
        messagebox.showinfo(welcome_window, 'update succesfully', command=check_if_in_db)
    else:
        messagebox.showinfo(welcome_window, 'not update, try again', command=check_if_in_db)


def update(*args):
    """the user choose wht to update and the function send to server"""
    frame_update_page = Frame(welcome_window, bg=color, highlightthickness=2, highlightbackground="#111")
    frame_update_page.place(relx=0.6, rely=0.2, relwidth=0.3, relheight=0.7)
    l_fname = Label(frame_update_page, text="choose what to update", bg=color, font=("impact", 30))
    l_fname.pack(side='top')

    global var, choose_var
    var = IntVar()

    r1 = Radiobutton(frame_update_page, text="first name", variable=var, value=1, font=('impact', 15), bg=color)
    r1.place(relx=0.2, rely=0.2, relwidth=0.2, relheight=0.1)

    r2 = Radiobutton(frame_update_page, text="last name", variable=var, value=2, font=('impact', 15), bg=color)
    r2.place(relx=0.2, rely=0.3, relwidth=0.2, relheight=0.1)

    r3 = Radiobutton(frame_update_page, text="user name", variable=var, value=3, font=('impact', 15), bg=color)
    r3.place(relx=0.2, rely=0.4, relwidth=0.2, relheight=0.1)

    r4 = Radiobutton(frame_update_page, text="password", variable=var, value=4, font=('impact', 15), bg=color)
    r4.place(relx=0.2, rely=0.5, relwidth=0.2, relheight=0.1)

    r5 = Radiobutton(frame_update_page, text="job", variable=var, value=5, font=('impact', 15), bg=color)
    r5.place(relx=0.2, rely=0.6, relwidth=0.2, relheight=0.1)

    l_update = Label(frame_update_page, text="enter the update:", bg=color, font=("impact", 15))
    l_update.place(relx=0.2, rely=0.75)

    choose_var = StringVar()
    update = Entry(frame_update_page, textvariable=choose_var, cursor="circle", bg='white', bd=3)
    update.place(relx=0.57, rely=0.75, relwidth=0.2, relheight=0.05)

    b11 = Button(frame_update_page, text='update', font=('impact', 15), bg=color, command=entry_update,
                 highlightthickness=2, highlightbackground=color)
    b11.place(relx=0.65, rely=0.4, relwidth=0.15, relheight=0.07)

    b12 = Button(frame_update_page, text='X', font=('impact', 15), bg=color, command=check_if_in_db)
    b12.place(relx=0, rely=0, relwidth=0.05, relheight=0.04)


def entry_update(*args):
    choose = var.get()
    input = choose_var.get()
    user_name = user_var.get()
    if choose == 0 or len(input) == 0:
        messagebox.showinfo(welcome_window, 'please choose what to update', command=update)
    else:
        path = "https://nati-server.herokuapp.com/update"
        info = {'choose': choose, 'input': input, 'user_name': user_name}
        answer = requests.get(url=path, params=info)
        ans1 = answer.content
        ans1 = ans1.decode("utf-8")
        if choose == 3 or choose == 4:
            messagebox.showinfo(welcome_window, 'update successfully, but you need to log in again', command=button_log_in)
        elif 'True' in ans1:
            messagebox.showinfo(welcome_window, 'update successfully', command=check_if_in_db)
        else:
            messagebox.showinfo(welcome_window, 'not update, try again', command=check_if_in_db)


def msg_method(*args):
    """frame that present the msg and from who"""
    """do a function that return how many users have in the db
    and according to that build the height of the list"""
    frame_msg = Frame(welcome_window, bg=color, highlightthickness=2, highlightbackground="#111")
    frame_msg.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)
    l_title = Label(frame_msg, text='CHATS', bg=color, font='impact')
    l_title.config(font=("impact", 44))
    l_title.pack(side='top')

    clock1 = Clock(frame_msg)
    clock1.place(relx=0.86, rely=0.9)
    clock1.configure(bg=color, fg='black', font=("helvetica", 35))

    label = Label(frame_msg, text='users names', bg=color, font=('impact', 25))
    label.place(relx=0.8, rely=0.05)
    users_lst = Listbox(frame_msg, selectmode=SINGLE, bg='#cce0ff', font=('impact', 15), bd=3)
    users_lst.place(relx=0.8, rely=0.1)
    """users names lists"""
    path = "https://nati-server.herokuapp.com/do_user_list"
    info = {}
    ans = requests.get(url=path, params=info)
    names = ans._content
    names = names.decode("utf-8")
    index = 0
    for label in names.split(','):
        label = label.replace('[', '')
        label = label.replace('"', '')
        label = label.replace(']', '')
        index = index + 1
        users_lst.insert(index, label)

    mb = Menubutton(frame_msg, text="OPTIONS", relief=RAISED, bg=color, font=('impact', 15))
    mb.place(relx=0.8, rely=0.43, relheight=0.1, relwidth=0.2)
    mb.menu = Menu(mb, tearoff=0)
    mb["menu"] = mb.menu

    sendvar = IntVar()
    showvar = IntVar()

    mb.menu.add_checkbutton(label="send new msg", variable=sendvar, font=('impact', 20), command=send_msg)
    mb.menu.add_checkbutton(label="show all msg", variable=showvar, font=('impact', 20), command=show_msg)

    b13 = Button(frame_msg, text='Back', font=('impact', 20), bg=color, command=check_if_in_db)
    b13.place(relx=0, rely=0, relwidth=0.05, relheight=0.05)


def send_msg(*args):
    """get the content of the user and which user to send
    #cce0ff"""
    frame_send_msg = Frame(welcome_window, bg=color, bd=4)
    frame_send_msg.place(relx=0.11, rely=0.35, relwidth=0.6, relheight=0.3)

    l_to = Label(frame_send_msg, text='to:', bg=color, font=('impact', 20))
    l_to.place(relx=0, rely=0.3)
    global to_var
    to_var = StringVar()
    to = Entry(frame_send_msg, textvariable=to_var, cursor="circle", bg='#cce0ff', bd=3)
    to.place(relx=0, rely=0.45, relwidth=0.1)

    l_content = Label(frame_send_msg, text='content:', bg=color, font=('impact', 20))
    l_content.place(relx=0, rely=0.6)
    global content_var
    content_var = StringVar()
    content = Entry(frame_send_msg, textvariable=content_var, cursor="circle", bg='#cce0ff', bd=3)
    content.place(relx=0, rely=0.75, relwidth=0.6, relheight=0.2)

    b11 = Button(frame_send_msg, text='send', font=('impact', 20), bg=color, command=insert)
    b11.place(relx=0.5, rely=0.6, relwidth=0.1, relheight=0.1)

    b11 = Button(frame_send_msg, text='X', font=('impact', 15), bg=color, command=msg_method)
    b11.place(relx=0, rely=0, relwidth=0.06, relheight=0.06)


def insert(*args):
    """send to the server request with the parameters and check if the user is ok if yes it insert to the db"""
    from_who = user_var.get()
    content = content_var.get()
    to = to_var.get()
    path = "https://nati-server.herokuapp.com/send_msg"
    info = {'content': content, 'to': to, 'from': from_who}
    ans = requests.get(url=path, params=info)
    ok = ans._content
    ok = ok.decode("utf-8")
    if 'didnt' in ok:
        messagebox.showinfo(welcome_window, 'the user name is incorrect', command=send_msg)
    else:
        messagebox.showinfo(welcome_window, 'send succesfully', command=msg_method)


def show_msg(*args):
    """show all message that send to this user"""
    frame_show_msg = Frame(welcome_window, bg=color, bd=4)
    frame_show_msg.place(relx=0.11, rely=0.35, relwidth=0.6, relheight=0.3)
    l_title = Label(frame_show_msg, text='INBOX', bg=color, font=('impact', 35))
    l_title.pack(side='top')

    to = user_var.get()
    path = "https://nati-server.herokuapp.com/all_msg"
    info = {'to': to}
    ans = requests.get(url=path, params=info)
    global msg_lst
    msg_lst = Listbox(frame_show_msg, selectmode=SINGLE, bg='#cce0ff', font=('impact', 15), bd=3)
    msg_lst.place(relx=0.2, rely=0.2, relwidth=0.50, relheight=0.35)
    msg = ans._content
    msg = msg.decode("utf-8")
    index = 0
    for label in msg.split('}'):
        label = label.replace('[', '')
        label = label.replace('{', '')
        label = label.replace('"', '')
        label = label.replace('content', '')
        label = label.replace(':', '')
        label = label.replace('"', '')
        label = label.replace('}', '')
        label = label.replace(']', '')
        index = index + 1
        msg_lst.insert(index, label)

    l_show = Label(frame_show_msg, text='sort from who', bg=color, font=('impact', 15))
    l_show.place(relx=0.2, rely=0.65)
    global show_var
    show_var = StringVar()
    show = Entry(frame_show_msg, textvariable=show_var, cursor="circle", bg='#cce0ff', bd=3)
    show.place(relx=0.2, rely=0.75)

    b14 = Button(frame_show_msg, text='REFRESH', font=('impact', 15), bg=color, command=show_msg, highlightthickness=2,
                 highlightbackground=color)
    b14.place(relx=0.6, rely=0.6, relwidth=0.08, relheight=0.08)

    b12 = Button(frame_show_msg, text='show', font=('impact', 15), bg=color, command=show_only_one)
    b12.place(relx=0.2, rely=0.9, relwidth=0.06, relheight=0.06)

    b11 = Button(frame_show_msg, text='X', font=('impact', 15), bg=color, command=msg_method)
    b11.place(relx=0, rely=0, relwidth=0.05, relheight=0.06)


def show_only_one(*args):
    show = show_var.get()
    user = user_var.get()
    path = "https://nati-server.herokuapp.com/show_only_user"
    info = {'show': show, 'to': user}
    msg_lst.delete(0, END)
    ans = requests.get(url=path, params=info)
    msg = ans._content
    msg = msg.decode("utf-8")
    index = 0
    for label in msg.split('}'):
        label = label.replace('[', '')
        label = label.replace('{', '')
        label = label.replace('"', '')
        label = label.replace('content', '')
        label = label.replace(':', '')
        label = label.replace('"', '')
        label = label.replace('}', '')
        label = label.replace(']', '')
        index = index + 1
        msg_lst.insert(index, label)


def button_sign_up(*args):
    """the sign up function entry user name, first name, last name, password, job and
    than use anther function to send the info to the server"""

    frame_sign_in = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_sign_in.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)
    l_title = Label(frame_sign_in, text='enter your details', bg='#80c1ff')
    frame_x = Frame(welcome_window, bg='lightblue', highlightthickness=2, highlightbackground="#111")
    frame_x.place(relx=0.1, rely=0.25, relwidth=0.8, relheight=0.4)
    l_title = Label(frame_sign_in, text='ENTER YOUR DETAILS', bg='#80c1ff', font=('impact', 35))
    l_title.pack(side='top')

    l_first_name = Label(frame_x, text='enter your first name', font='impact', bg='lightblue', cursor='circle')
    l_first_name.config(font=("impact", 20))
    l_first_name.place(relx=0.3, rely=0.1, relwidth=0.15, relheight=0.15)

    global first_name_var
    first_name_var = StringVar()
    first_name = Entry(frame_x, textvariable=first_name_var, cursor="circle", bg='lightblue')
    first_name.place(relx=0.3, rely=0.25)

    l_last_name = Label(frame_x, text='enter your last name', font='impact', bg='lightblue', cursor='circle')
    l_last_name.config(font=("impact", 20))
    l_last_name.place(relx=0.3, rely=0.35, relwidth=0.15, relheight=0.15)

    global last_name_var
    last_name_var = StringVar()
    last_name = Entry(frame_x, textvariable=last_name_var, cursor="circle", bg='lightblue')
    last_name.place(relx=0.3, rely=0.5)

    l_user_name = Label(frame_x, text='what user name you want', font='impact', bg='lightblue', cursor='circle')
    l_user_name.config(font=("impact", 20))
    l_user_name.place(relx=0.29, rely=0.65, relwidth=0.2, relheight=0.15)

    global user_name_var
    user_name_var = StringVar()
    user_name = Entry(frame_x, textvariable=user_name_var, cursor="circle", bg='lightblue')
    user_name.place(relx=0.3, rely=0.8)

    l_real_password = Label(frame_x, text='enter password', font='impact', bg='lightblue', cursor='circle')
    l_real_password.config(font=("impact", 20))
    l_real_password.place(relx=0.59, rely=0.1, relwidth=0.15, relheight=0.15)

    global real_password_var
    real_password_var = StringVar()
    real_password = Entry(frame_x, textvariable=real_password_var, cursor="circle", bg='lightblue', show="*")
    real_password.place(relx=0.6, rely=0.25)

    l_repeat_password = Label(frame_x, text='repeat password', font='impact', bg='lightblue', cursor='circle')
    l_repeat_password.config(font=("impact", 20))
    l_repeat_password.place(relx=0.59, rely=0.35, relwidth=0.15, relheight=0.15)

    global repeat_password_var
    repeat_password_var = StringVar()
    repeat_password = Entry(frame_x, textvariable=repeat_password_var, cursor="circle", bg='lightblue', show="*")
    repeat_password.place(relx=0.6, rely=0.5)

    l_job = Label(frame_x, text='enter your job', font='impact', bg='lightblue', cursor='circle')
    l_job.config(font=("impact", 20))
    l_job.place(relx=0.59, rely=0.65, relwidth=0.15, relheight=0.15)

    global job_var
    job_var = StringVar()
    job = Entry(frame_x, textvariable=job_var, cursor="circle", bg='lightblue')
    job.place(relx=0.6, rely=0.8)

    b10 = Button(frame_sign_in, text="sign up", height=5, width=5, bg='#b3c6ff', bd=5,
                 font=('impact', 20), highlightthickness=5, highlightbackground="#b3c6ff", command=sign_up_request)
    b10.place(relx=0.38, rely=0.7, relwidth=0.1, relheight=0.1)

    b11 = Button(frame_sign_in, text='Back', font=('impact', 20), bg='#80c1ff', command=main)
    b11.place(relx=0, rely=0, relwidth=0.05, relheight=0.05)


def sign_up_request(*args):
    """get all the info from the user and check if already the db has the same user name like the user name that the user name gave,
    check if the password and the repeat password are the same and than send request to insert the info to the db """

    frame_sign_up = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_sign_up.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)
    l_title = Label(frame_sign_up, text='enter your details', bg='#80c1ff')
    l_title.config(font=("impact", 44))
    l_title.pack(side='top')

    first_name = first_name_var.get()
    last_name = last_name_var.get()
    user_name = user_name_var.get()
    password = real_password_var.get()
    passwordx = repeat_password_var.get()
    job = job_var.get()
    color = '#80c1ff'

    if len(first_name) == 0 or len(last_name) == 0 or len(user_name) == 0 or len(password) == 0 or len(job) == 0:
        messagebox.showinfo(frame_sign_up, 'one of the details are empty', command=button_sign_up)
    else:
        path = "https://nati-server.herokuapp.com/if_has_user"
        info = {'user-name': user_name}
        ans = requests.get(url=path, params=info)
        decoded = ans._content.decode("utf-8")
        if 'dont have' == decoded:
            if password == passwordx:
                path = "https://nati-server.herokuapp.com/sign_up_route"
                info = {'first-name': first_name, 'last-name': last_name, 'user-name': user_name, 'password': password,
                        'job': job, 'clearance': 4, 'color': color}
                ans1 = requests.get(url=path, params=info)
                messagebox.showinfo(frame_sign_up, 'sign up successfully, please log in', command=main)
            else:
                messagebox.showinfo(frame_sign_up, 'the passwords don`t match', command=button_sign_up)
        else:
            messagebox.showinfo(frame_sign_up, 'there already that user name, please choose anther on',
                                command=button_sign_up)


def admin_button(*args):
    """admin zone"""
    frame_admin = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_admin.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)

    frame_y = Frame(welcome_window, bg='lightblue', highlightthickness=2, highlightbackground="#111")
    frame_y.place(relx=0.1, rely=0.25, relwidth=0.8, relheight=0.3)

    l = Label(frame_admin, text='SUPER USER', bg='#80c1ff', font=("impact", 40, 'bold'))
    l.pack(side='top')

    l_name = Label(frame_y, text='user name:', font=("impact", 20), bg='lightblue', highlightthickness=5,
                   highlightbackground="#111")
    l_name.place(relx=0.34, rely=0.02, relwidth=0.3, relheight=0.2)

    global user_var_admin
    user_var_admin = StringVar()
    user_var_admi = Entry(frame_y, textvariable=user_var_admin, bg='white')
    user_var_admi.place(relx=0.4, rely=0.2)

    l_password = Label(frame_y, text='password:', bd=3, font=("impact", 20), bg='lightblue', highlightthickness=5,
                       highlightbackground="#b3c6ff")
    l_password.place(relx=0.34, rely=0.35, relwidth=0.3, relheight=0.2)

    global pass_var_admin
    pass_var_admin = StringVar()
    pass_var_admi = Entry(frame_y, textvariable=pass_var_admin, bg='white', show="*")
    pass_var_admi.place(relx=0.4, rely=0.55)

    b10 = Button(frame_y, text="continue", font=('impact', 20), command=check_if_in_db_admin, bg='lightblue',
                 highlightthickness=2, highlightbackground="lightblue")
    b10.place(relx=0.43, rely=0.7, relwidth=0.1, relheight=0.1)

    b11 = Button(frame_admin, text='Back', font=('impact', 20), bg='#80c1ff', command=main)
    b11.place(relx=0, rely=0, relwidth=0.05, relheight=0.05)


def check_if_in_db_admin(*args):
    global color
    """the continue that of the button_log_in. this function send to the server the user name and the password
    and present the appropriate message"""
    u_name = user_var_admin.get()
    password = pass_var_admin.get()
    path = "https://nati-server.herokuapp.com/admin"
    info = {'user-name': u_name, 'password': password}
    ans = requests.get(url=path, params=info)
    ans = ans.content
    dict_str = ans.decode("UTF-8")
    if dict_str != 'no user':
        mydata = (dict_str)
        open_admin_page(mydata)
    else:
        messagebox.showinfo(welcome_window, 'you dont have user, please register or try again to log in', command=admin_button)


def open_admin_page(mydata):
    frame_admin_page = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_admin_page.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)
    title = Label(frame_admin_page, text='WELCOME BACK', takefocus=0, wraplength=599, font=("impact", 35, 'bold'),
                  bg='#80c1ff')
    title.pack(side='top')
    users_lst = Listbox(frame_admin_page, selectmode=SINGLE, bg='#cce0ff', font=('impact', 15), bd=3)
    users_lst.place(relx=0.5, rely=0.6, relheight=0.4, relwidth=0.5)
    names = mydata
    index = 0
    for label in names.split('}'):
        label = label.replace('[', '')
        label = label.replace('{', '')
        label = label.replace('"', '')
        label = label.replace(':', '')
        label = label.replace('first-name', '')
        label = label.replace('"', '')
        label = label.replace('"', '')
        label = label.replace('"', '')
        label = label.replace(']', '')
        label = label.replace('}', '')
        label = label.replace('last-name', '')
        label = label.replace('password', '')
        label = label.replace('user-name', '')
        label = label.replace('job', '')
        label = label.replace('clearance', '')
        index = index + 1
        users_lst.insert(index, label)
    l1 = Label(frame_admin_page, text='F-name', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l1.place(relx=0.5, rely=0.55)

    l2 = Label(frame_admin_page, text='L-name', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l2.place(relx=0.56, rely=0.55)

    l3 = Label(frame_admin_page, text='password', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l3.place(relx=0.62, rely=0.55)

    l4 = Label(frame_admin_page, text='U-name', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l4.place(relx=0.69, rely=0.55)

    l5 = Label(frame_admin_page, text='job', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l5.place(relx=0.75, rely=0.55)

    l6 = Label(frame_admin_page, text='clearance', takefocus=0, wraplength=599, font=("impact", 15, 'bold'),
               bg='#80c1ff')
    l6.place(relx=0.8, rely=0.55)
    index = index -1
    l99 = Label(frame_admin_page, text='Active users: %s' % index, takefocus=0, wraplength=599,
                font=("impact", 25, 'bold'),
                bg='#80c1ff')
    l99.place(relx=0.8, rely=0.4)

    mb = Menubutton(frame_admin_page, text="OPTIONS", relief=RAISED, bg='#80c1ff', font=('impact', 20))
    mb.place(relx=0.8, rely=0.2, relheight=0.1, relwidth=0.2)
    mb.menu = Menu(mb, tearoff=0)
    mb["menu"] = mb.menu

    changevar = IntVar()
    msgvar = IntVar()

    mb.menu.add_checkbutton(label="Delete users", variable=changevar, font=('impact', 20), command=delete)
    mb.menu.add_checkbutton(label="Update users details", variable=msgvar, font=('impact', 20), command=update_admin)
    mb.menu.add_checkbutton(label="LOG OUT", variable=msgvar, font=('impact', 20), command=main)


def delete(*args):
    frame_admin_delete = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_admin_delete.place(relx=0.1, rely=0.6, relwidth=0.3, relheight=0.3)
    l99 = Label(frame_admin_delete, text='write the user name to delete', takefocus=0, wraplength=599,
                font=("impact", 15, 'bold'),
                bg='#80c1ff')
    l99.place(relx=0.3, rely=0.3)

    global user_var_admin_delete
    user_var_admin_delete = StringVar()
    user_var_admi1 = Entry(frame_admin_delete, textvariable=user_var_admin_delete, bg='white')
    user_var_admi1.place(relx=0.3, rely=0.4)

    b22 = Button(frame_admin_delete, text='delete', font=('impact', 20), bg='#80c1ff', command=delete_user)
    b22.place(relx=0.37, rely=0.55, relwidth=0.15, relheight=0.15)

    b22 = Button(frame_admin_delete, text='X', font=('impact', 20), bg='#80c1ff', command=open_admin_page)
    b22.place(relx=0, rely=0, relwidth=0.09, relheight=0.09)


def delete_user(*args):
    user_name = user_var_admin_delete.get()
    path = "https://nati-server.herokuapp.com/delete"
    info = {'user-name': user_name}
    ans = requests.get(url=path, params=info)
    ans = ans.content
    dict_str = ans.decode("UTF-8")
    if dict_str != '####':
        messagebox.showinfo(welcome_window, 'delete successfully', command=check_if_in_db_admin)
    else:
        messagebox.showinfo(welcome_window, 'the user name is incorrect', command=delete)


def update_admin(*args):
    """the user choose what to update and the function send to server"""
    frame_update_page = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_update_page.place(relx=0.1, rely=0.2, relwidth=0.3, relheight=0.7)
    l_fname = Label(frame_update_page, text="choose what to update", bg='#80c1ff', font=("impact", 30))
    l_fname.pack(side='top')

    global var, choose_var, to_var_admin
    var = IntVar()

    r1 = Radiobutton(frame_update_page, text="first name", variable=var, value=1, font=('impact', 15), bg='#80c1ff')
    r1.place(relx=0.2, rely=0.12, relwidth=0.2, relheight=0.1)

    r2 = Radiobutton(frame_update_page, text="last name", variable=var, value=2, font=('impact', 15), bg='#80c1ff')
    r2.place(relx=0.2, rely=0.22, relwidth=0.2, relheight=0.1)

    r3 = Radiobutton(frame_update_page, text="user name", variable=var, value=3, font=('impact', 15), bg='#80c1ff')
    r3.place(relx=0.2, rely=0.32, relwidth=0.2, relheight=0.1)

    r4 = Radiobutton(frame_update_page, text="password", variable=var, value=4, font=('impact', 15), bg='#80c1ff')
    r4.place(relx=0.2, rely=0.42, relwidth=0.2, relheight=0.1)

    r5 = Radiobutton(frame_update_page, text="job", variable=var, value=5, font=('impact', 15), bg='#80c1ff')
    r5.place(relx=0.2, rely=0.52, relwidth=0.2, relheight=0.1)

    r6 = Radiobutton(frame_update_page, text="clearance", variable=var, value=6, font=('impact', 15), bg='#80c1ff')
    r6.place(relx=0.2, rely=0.62, relwidth=0.2, relheight=0.1)

    l_update = Label(frame_update_page, text="enter the update:", bg='#80c1ff', font=("impact", 15))
    l_update.place(relx=0.2, rely=0.74)

    choose_var = StringVar()
    update = Entry(frame_update_page, textvariable=choose_var, bg='white', bd=3)
    update.place(relx=0.57, rely=0.74, relwidth=0.2, relheight=0.05)

    to = Label(frame_update_page, text="enter the user:", bg='#80c1ff', font=("impact", 15))
    to.place(relx=0.2, rely=0.85)

    to_var_admin = StringVar()
    to1 = Entry(frame_update_page, textvariable=to_var_admin, bg='white', bd=3)
    to1.place(relx=0.57, rely=0.85, relwidth=0.2, relheight=0.05)

    b11 = Button(frame_update_page, text='update', font=('impact', 15), bg='#80c1ff', command=entry_update_admin,
                 highlightthickness=2, highlightbackground='#80c1ff')
    b11.place(relx=0.65, rely=0.4, relwidth=0.15, relheight=0.07)

    b12 = Button(frame_update_page, text='X', font=('impact', 15), bg='#80c1ff', command=check_if_in_db_admin)
    b12.place(relx=0, rely=0, relwidth=0.05, relheight=0.04)


def entry_update_admin(*args):
    choose = var.get()
    input = choose_var.get()
    user_name = to_var_admin.get()
    if choose == 0 or len(input) == 0:
        messagebox.showinfo(welcome_window, 'please choose what to update', command=check_if_in_db_admin)
    else:
        if choose == 6:
            if input == 2 or input == 3 or input == 4 or input == '2' or input == '3' or input == '4':
                path = "https://nati-server.herokuapp.com/update"
                info = {'choose': choose, 'input': input, 'user_name': user_name}
                answer = requests.get(url=path, params=info)
                ans1 = answer.content
                ans1 = ans1.decode("utf-8")
                if 'True' in ans1:
                    messagebox.showinfo(welcome_window, 'update successfully', command=check_if_in_db_admin)
                else:
                    messagebox.showinfo(welcome_window, 'not update, try again check the user name', command=check_if_in_db_admin)
            else:
                messagebox.showinfo(welcome_window, 'clearance could be only 2, 3, 4', command=check_if_in_db_admin)
        else:
            path = "https://nati-server.herokuapp.com/update"
            path2 = 'http://127.0.0.1:5000/update'
            info = {'choose': choose, 'input': input, 'user_name': user_name}
            answer = requests.get(url=path, params=info)
            ans1 = answer.content
            ans1 = ans1.decode("utf-8")
            if 'True' in ans1:
                messagebox.showinfo(welcome_window, 'update successfully', command=check_if_in_db_admin)
            else:
                messagebox.showinfo(welcome_window, 'not update, try again', command=check_if_in_db_admin)


def about(*args):
    """the about page"""
    frame_about = Frame(welcome_window, bg='#80c1ff')
    frame_about.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)
    l_title = Label(frame_about, text='welcome to about page', bg='#80c1ff')
    l_title.config(font=("impact", 44))
    l_title.pack(side='top')
    l_exp1 = Label(frame_about,
                   text='this is a program, all of the users that in the system can write messages to each other, and see.',
                   bg='#80c1ff')
    l_exp1.config(font=("impact", 20))
    l_exp1.place(relx=0.16, rely=0.2)

    l_exp2 = Label(frame_about,
                   text='all the messages the sent to him and from who, also each user can send direct message to the admin.',
                   bg='#80c1ff')
    l_exp2.config(font=("impact", 20))
    l_exp2.place(relx=0.16, rely=0.25)

    l_exp3 = Label(frame_about,
                   text='the admin have the highest clearance and the admin mange also the Data Base .',
                   bg='#80c1ff')
    l_exp3.config(font=("impact", 20))
    l_exp3.place(relx=0.16, rely=0.3)

    b22 = Button(frame_about, text='Back', font=('impact', 20), bg='#80c1ff', command=main)
    b22.place(relx=0, rely=0, relwidth=0.05, relheight=0.05)


def main(*args):
    """the main"""
    frame_main = Frame(welcome_window, bg='#80c1ff', highlightthickness=2, highlightbackground="#111")
    frame_main.place(relx=0.1, rely=0.1, relwidth=0.8, relheight=0.8)

    frame_title = Frame(frame_main, bg='#b3c6ff', bd=5, highlightthickness=2, highlightbackground="#111")
    frame_title.place(relx=0, rely=0, relheight=0.2, relwidth=1)

    frame_buttons = Frame(welcome_window, bg='#b3c6ff', highlightthickness=2, highlightbackground="#111")
    frame_buttons.place(relx=0.1, rely=0.1, relheight=0.8, relwidth=0.1)

    l_main = Label(frame_title, text='welcome', bg='#b3c6ff', width=22, height=15, takefocus=0, wraplength=170,
                   font=("impact", 44), justify=LEFT)
    l_main.pack(side='bottom')

    l_exp = Label(frame_main, text='if you already have user log in, if not please register', takefocus=0,
                  wraplength=599, font=("impact", 22),
                  bg='#b3c6ff')
    l_exp.place(relx=0.12, rely=0.2)

    b1 = Button(frame_buttons, text="LOG IN", command=button_log_in, height=5, width=5, bg='#b3c6ff', relief=RAISED,
                bd=5,
                font=('impact', 20), highlightthickness=5, highlightbackground="#b3c6ff")
    b1.place(relx=0.1, rely=0.32, relwidth=0.8, relheight=0.1)

    b2 = Button(frame_buttons, text="SIGN UP", command=button_sign_up, height=5, width=5, bg='#b3c6ff', relief=RAISED,
                bd=5, font=('impact', 20), highlightthickness=5, highlightbackground="#b3c6ff")
    b2.place(relx=0.1, rely=0.44, relwidth=0.8, relheight=0.1)

    b3 = Button(frame_buttons, text="Admin only", height=5, width=5, bg='#b3c6ff', relief=RAISED, bd=5,
                command=admin_button,
                font=('impact', 20), highlightthickness=5, highlightbackground="#b3c6ff")
    b3.place(relx=0.1, rely=0.56, relwidth=0.8, relheight=0.1)

    b4 = Button(frame_buttons, text="About", command=about, height=5, width=5, bg='#b3c6ff', bd=5,
                font=('impact', 20), highlightthickness=5, highlightbackground="#b3c6ff")
    b4.place(relx=0.1, rely=0.68, relwidth=0.8, relheight=0.1)


welcome_window = Tk(screenName=None, baseName=None, className='Main window', useTk=10)
canvas = Canvas(welcome_window, height=700, width=800)
welcome_window.geometry('1500x1068')
main()
welcome_window.mainloop()

"commit and push to GIT LAB"
